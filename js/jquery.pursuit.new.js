(function ( $ ) {
    $.fn.pursuit = function( options ) {

        var _ = this;

        var settings = $.extend({
            items: [
                {
                    class: '.headline1',
                    itemAnchor: 0,
                    timeline: [
                        {
                            trigger: 'item',
                            triggerAnchor: 0,
                            viewportAnchor: 50,
                            actions: [
                                {
                                    fix: true,
                                    opacity: '',
                                    opacityDuration: '',
                                    opacityEase: ''
                                }
                            ]
                        }
                    ]
                }
            ]
        }, options );


        $.map(settings.items, function(value) {
            //item values
            const itemClass = value.class;
            const item = _.find(itemClass);
            const itemOffset = item.offset().top;

            //item anchor point
            var itemAnchor = value.itemAnchor;
            itemAnchor = getAnchor(itemAnchor, item.outerHeight(true));

            //timeline
            const timeline = value.timeline;
            $.map(timeline, function(value) {
                //trigger
                const trigger = value.trigger;
                const triggerOffset = getTrigger(trigger, item);

                //viewport anchor point
                var viewportAnchor = value.viewportAnchor;
                viewportAnchor = getAnchor(viewportAnchor, $(window).height());

                //timeline actions
                const actions = value.actions;
                var fix = '';
                var itemOpacity = item.css('opacity');
                var opacity = '';
                var opacityDuration = '';
                var opacityEase = '';

                //action values
                $.map(actions, function(value) {
                    fix = value.fix;
                    opacity = value.opacity;
                    opacityDuration = value.opacityDuration;
                    opacityEase = value.opacityEase;
                });

                //fix action
                triggerActions(fix, item, itemOffset, itemAnchor, trigger, triggerOffset, viewportAnchor, opacity, opacityDuration, opacityEase, itemOpacity);
            });

        });

        function getAnchor(value, object) {
            value = object / 100 * value;
            return value;
        }

        function getTrigger(value, item) {
            if($.isNumeric(value)) {
                value = value;
                return value;
            } else if (value === 'item') {
                value = $(item).offset().top;
                return value;
            } else {
                value = $(value).offset().top;
                return value;
            }
        }

        function triggerActions(fix, item, itemOffset, itemAnchor, trigger, triggerOffset, viewportAnchor, opacity, opacityDuration, opacityEase, itemOpacity) {
            $(window).scroll(function() {
                const scrollOffset = $(window).scrollTop();
                $('.scrollPosition').text(scrollOffset);
                if(trigger === 'item') {
                    if (fix === true) {
                        if (scrollOffset + viewportAnchor >= triggerOffset + itemAnchor) {
                            item.css({
                                position: 'relative',
                                top: scrollOffset - triggerOffset + viewportAnchor - itemAnchor
                            })
                        } else {
                            item.css({
                                position: 'static',
                                top: 0
                            })
                        }
                    } else if (fix === false) {
                        if(scrollOffset + viewportAnchor >= triggerOffset + itemAnchor) {
                            if(!item.hasClass('isFix')) {
                                var latestPosition = item.css('top');
                                item.addClass('isFix');
                                item.data('latestPosition', latestPosition);
                            }
                            item.css({
                                position: 'relative',
                                top: item.data('latestPosition')
                            })
                        } else {
                            item.css({
                                position: 'static',
                                top: 0
                            })
                        }
                    }
                    if(opacity) {
                        if (scrollOffset + viewportAnchor >= triggerOffset + itemAnchor) {
                            if(!item.hasClass('isOpacity')) {
                                var latestOpacity = opacity;
                                item.addClass('isOpacity');
                                item.data('latestOpacity', latestOpacity);

                                item.animate({
                                    opacity: latestOpacity
                                }, opacityDuration);
                            }
                        } else {
                            item.removeClass('isOpacity');
                            item.animate({
                                opacity: itemOpacity
                            }, opacityDuration);
                        }
                    }
                } else {
                    if (fix === true) {
                        if (scrollOffset >= triggerOffset + viewportAnchor) {
                            var fullOffset = (itemOffset > (triggerOffset + viewportAnchor)) ? (triggerOffset + viewportAnchor) : itemOffset;
                            item.css({
                                position: 'relative',
                                top: scrollOffset - fullOffset
                            })
                        } else {
                            item.css({
                                position: 'static',
                                top: 0
                            })
                        }
                    }else if (fix === false) {
                        if(scrollOffset + viewportAnchor >= triggerOffset - itemAnchor) {
                            if(!item.hasClass('isFix')) {
                                var latestPosition = item.css('top');
                                item.addClass('isFix');
                                item.data('latestPosition', latestPosition);
                            }
                            item.css({
                                position: 'relative',
                                top: item.data('latestPosition'),
                            })
                        }
                    }
                    if(opacity) {
                        if (scrollOffset >= triggerOffset + viewportAnchor) {
                            if(!item.hasClass('isOpacity')) {
                                var latestOpacity = opacity;
                                item.addClass('isOpacity');
                                item.data('latestOpacity', latestOpacity);

                                item.animate({
                                    opacity: latestOpacity
                                }, opacityDuration);
                            }
                        } else {
                            item.removeClass('isOpacity');
                            item.animate({
                                opacity: itemOpacity
                            }, opacityDuration);
                        }
                    }
                }
            });
        }
    }

}( jQuery ));