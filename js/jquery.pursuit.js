(function ( $ ) {
    $.fn.pursuitOld = function( options ) {

        var _ = this;

        var settings = $.extend({
            test: 'Hello',
            items: [
                {
                    class: '.headline1',
                    anchor: 'top',
                    timeline: [
                        {
                            trigger: 0.5,
                            action: 'fix',
                            effect: '',
                            reference: [{
                                type: 'viewport',
                                class: '',
                                anchor: 'top'
                            }]
                        }
                    ]
                }
            ]
        }, options );


        $.map(settings.items, function(value) {
            //item values
            const itemClass = value.class;
            const item = _.find(itemClass);
            const itemOffset = item.offset().top;

            //item anchor point
            var itemAnchor = value.anchor;
            itemAnchor = getAnchor(itemAnchor, item.outerHeight(true));

            //timeline
            const timeline = value.timeline;
            $.map(timeline, function(value) {
                //timeline values
                const trigger = value.trigger;
                const action = value.action;

                const effect = value.effect;

                //timeline reference
                const ref = value.reference;
                var refType = '';
                var refClass = '';
                var refAnchor =  '';

                //reference values
                $.map(ref, function(value) {
                    refType = value.type;
                    refClass = value.class;
                    refAnchor = value.anchor;
                });

                //reference anchor point
                if(refType === 'viewport') {
                    refAnchor = getAnchor(refAnchor, $(window).height());
                }
                if(refType === 'document') {
                    refAnchor = getAnchor(refAnchor, $(document).height());
                }
                if(refType === 'object') {
                    refAnchor = getAnchor(refAnchor, $(refClass).height());
                }

                //scrolling function
                scrolling(action, item, itemAnchor, itemOffset, trigger, refAnchor);
            });



        });

        function getAnchor(value, object) {
            if(value === 'top') {
                value = 0;
                return value;
            }
            if(value === 'middle') {
                value = object / 2;
                return value;
            }
            if(value === 'bottom') {
                value = object;
                return value;
            }
        }

        function scrolling(action, item, itemAnchor, itemOffset, trigger, refAnchor) {
            $(window).scroll(function() {
                const scrollOffset = $(window).scrollTop();

                if(action === 'fix') {
                    if(scrollOffset + ($(window).height() * trigger) + refAnchor >= itemOffset + itemAnchor) {
                        item.css({
                            position: 'relative',
                            top: scrollOffset + ($(window).height() * trigger) - refAnchor - itemOffset - itemAnchor
                        })
                    } else {
                        item.css({
                            position: 'static',
                            top: 0
                        })
                    }
                }

                if(action === 'static') {
                    if(scrollOffset + ($(window).height() * trigger) + refAnchor >= itemOffset + itemAnchor) {

                        if(!item.hasClass('isActive')) {
                            var latestPosition = scrollOffset + ($(window).height() * trigger) - refAnchor - itemOffset - itemAnchor;
                            item.addClass('isActive');
                            item.data('latestPosition', latestPosition);
                        }


                        item.css({
                            position: 'relative',
                            top: item.data('latestPosition')
                        })
                    }
                }
            });
        }
    }

}( jQuery ));