$('.wrapper').pursuit({
    items: [
        {
            class: '.headline1',
            itemAnchor: 0,
            timeline: [
                {
                    trigger: 'item',
                    viewportAnchor: 50,
                    actions: [
                        {
                            fix: true,
                            opacity: 1,
                            opacityDuration: 0.3,
                            opacityEase: 'swing'
                        }
                    ]
                },
                {
                    trigger: '.headline2',
                    viewportAnchor: 50,
                    actions: [
                        {
                            fix: false,
                            opacity: 0.5,
                            opacityDuration: 0.3,
                            opacityEase: 'swing'
                        }
                    ]
                }
            ]
        },
        {
            class: '.headline2',
            itemAnchor: 0,
            timeline: [
                {
                    trigger: 'item',
                    viewportAnchor: 50,
                    actions: [
                        {
                            fix: true,
                            opacity: 1,
                            opacityDuration: 0.3,
                            opacityEase: 'swing'
                        }
                    ]
                },
                {
                    trigger: '.headline3',
                    viewportAnchor: 50,
                    actions: [
                        {
                            fix: false,
                            opacity: 0.5,
                            opacityDuration: 0.3,
                            opacityEase: 'swing'
                        }
                    ]
                }
            ]
        },
        {
            class: '.headline3',
            itemAnchor: 0,
            timeline: [
                {
                    trigger: 'item',
                    viewportAnchor: 50,
                    actions: [
                        {
                            fix: true,
                            opacity: 1,
                            opacityDuration: 0.3,
                            opacityEase: 'swing'
                        }
                    ]
                },
                {
                    trigger: 4000,
                    viewportAnchor: 50,
                    actions: [
                        {
                            fix: false,
                            opacity: 0.5,
                            opacityDuration: 0.3,
                            opacityEase: 'swing'
                        }
                    ]
                }
            ]
        }
    ]
});