(function ( $ ) {
	$.fn.pursuit = function( options ) {

		var _ = this;

		var settings = $.extend({
			test: 'Hello',
			items: [
				{
					class: '.headline1',
					anchor: 'top',
					timeline: [
						{
							trigger: 0.5,
							reference: [{
								type: 'viewport',
								anchor: 'middle'
							}],
							action: 'fix',
							effect: ''
						}
					]
				},
				{
					class: '.headline2',
					anchor: 'middle',
					timeline: [
						{
							trigger: 0.5,
							reference: [{
								type: 'viewport',
								anchor: 'top'
							}],
							action: 'fix',
							effect: ''
						}
					]
				},
				{
					class: '.headline3',
					anchor: 'bottom',
					timeline: [
						{
							trigger: 0.5,
							reference: [{
								type: 'viewport',
								anchor: 'top'
							}],
							action: 'fix',
							effect: ''
						}
					]
				}
			]
		}, options );


		$.map(settings.items, function(value) {
			const itemClass = value.itemClass;
			const item = _.find(itemClass);
			const itemHeight = item.outerHeight(true);
			const itemOffset = item.offset().top;

			//define the anchor point of the item
			var itemAnchor = value.itemAnchor;
			if(itemAnchor === 'top') {
				itemAnchor = 0;
			}
			if(itemAnchor === 'middle') {
				itemAnchor = itemHeight / 2;
			}
			if(itemAnchor === 'bottom') {
				itemAnchor = itemHeight;
			}

			$('#debug').append('<br><p><b>itemClass:' + itemClass + '</b><br> itemOffset: ' + itemOffset + '</b><br> itemHeight: ' + itemHeight + '<br> itemAnchor: ' + itemAnchor + '<br></p>');

			$(window).scroll(function() {
				const windowTop = $(window).scrollTop();
				const windowHeight = $(window).outerHeight(true);

				const timeline = value.timeline;
				$.map(timeline, function(value) {

					var trigger = value.trigger;
					const reference = value.reference;
					//const action = value.action;
					//const effect = value.effect;

					if(reference === 'viewport') {
						trigger = windowHeight * trigger;
						if(windowTop + trigger >= itemOffset + itemAnchor) {
							item.css({
								position: 'relative',
								top: windowTop + trigger - itemOffset + itemAnchor
							})
						} else {
							item.css({
								position: 'static',
								top: 0
							})
						}
					}

				});
			});

		});

		function anchor(value, type) {
			if(value === 'top') {
				value = 0;
				return value;
			}
			if(value === 'middle') {
				value = type.outerHeight(true) / 2;
				return value;
			}
			if(value === 'bottom') {
				value = type.outerHeight(true);
				return value;
			}
		}
	};

}( jQuery ));